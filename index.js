const WebSocket = require('ws');

const teeworlds = require('teeworlds');

let client = new teeworlds.Client("45.141.57.154", 8303, "web client");
client.connect();

client.on("connected", () => {
	client.game.Say("/showall");
})

function broadcastMessage(data) {
	wss.clients.forEach(ws_client => {
		if (ws_client.readyState === WebSocket.OPEN) {
		//   client.send('Hello! A new client has connected.');
			ws_client.send(data)
		//    client.send(JSON.stringify({event: "snap", data: items}));
		//    client.send(JSON.stringify({event: "tee_at", data: {x: Math.random() * 1920, y: Math.random() * 1080}}))
		}
	})

}

client.on("snapshot", (items) => {
	broadcastMessage(JSON.stringify({event: "snap", data: {"playerinfo": client.SnapshotUnpacker.AllObjPlayerInfo, "characters": client.SnapshotUnpacker.AllObjCharacter, "clientinfo": client.SnapshotUnpacker.AllObjClientInfo}}));
})
client.on("message", (msg) => {
	broadcastMessage(JSON.stringify({event: "chat", data: msg}))
})

const wss = new WebSocket.Server({ port: 8080 });

wss.on('connection', ws => {
  console.log('New client connected');

  // Broadcast message to all connected clients
  wss.clients.forEach(client => {
    if (client.readyState === WebSocket.OPEN) {
    //   client.send('Hello! A new client has connected.');
	   client.send(JSON.stringify({event: "tee_at", data: {x: Math.random() * 1920, y: Math.random() * 1080}}))
    }
  });
  ws.on("message", (msg) => {
	let json;
	try {
		json = JSON.parse(msg.toString());
	} catch (e) {};
	if (json.event === "move") {
		// if (json.)
		client.movement.input = json.data;
		client.sendInput();
	} else if (json.event === "kill") {
		client.game.Kill();
	}
  })
  ws.on('close', () => {
    console.log('Client disconnected');
  });
});

process.on("SIGINT", () => {
	client.Disconnect().then(() => process.exit(0)); // disconnect on ctrl + c
	// process.exit()
})